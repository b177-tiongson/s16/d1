//Repetition Control Structures

//While Loop

/*SYNTAX

while(condition){
	statement/s;
}

*/

let count = 5;

while(count !== 0){
	console.log("While: " +count);
	count--;
};


console.log("Displays number 1-10")

countTwo = 1;
while(countTwo < 11){
	console.log("While: " +countTwo);
	countTwo++;

}

//DO WHILE LOOP

/* SYNTAX


do{
	statement;
}while(condition);

*/

//Number() is similar to parseInt when converting String to numbers

let number = Number(prompt("Give me a number: "));


do{
	console.log("Do While: " +number);
	number +=1;
}while(number <10);


let x = 1;

do{
	if((x%2)===0){
		console.log("Even Number: "+x);
	};
	x++;

}while(x<11);


//FOR LOOP

/*SYNTAX

for(initialization;condition;stepExpression){
	
	statement;
}

*/

console.log("FOR LOOP")

for(let cnt = 0; cnt<=20; cnt++){
	console.log(cnt);
};

console.log("EVEN FORLOOP");


let even = 2;
for(let counter=1;counter<=5; counter++ ){
	console.log("EVEN: "+even);
	even += 2;
}

console.log("------------------------------")
let myString = 'alex'
//.lenth property is used to count the characters in a string
console.log(myString.length);

console.log(myString[0]);

for(z=0; z<myString.length;z++){
	console.log(myString[z]);
}


for(a=myString.length-1; a>=0; a--){
	console.log(myString[a]);
}

console.log("PRINT OUT LETTER INDIVIDUALLY BUT WILL PRINT 3 INSTEAD OF THE VOWELS")
let myName="AlEx";

for(let b = 0; b<myName.length; b++){



	if(myName[b].toLowerCase()== "a" ||
		myName[b].toLowerCase()=="e" ||
		myName[b].toLowerCase()=="i" ||
		myName[b].toLowerCase()=="u")
	{

		console.log("3");
	}
	else{
		console.log(myName[b]);
	};

};

//Continue and Break Statements

for(let count123 = 0; count123<=20; count123++){
	//if the remainder is equal to 0, tells the code to continue to iterate

	if(count123%2===0){
		continue;
	};
	console.log("continue and break: " +count123);

	if(count123>10){
		break;
	}


}





